package mogog

import (
	"reflect"

	"gitlab.com/Ganiyamustafa/mogog/hook"
	"gitlab.com/Ganiyamustafa/mogog/option"
	"gitlab.com/Ganiyamustafa/mogog/utils"
)

type Hookable interface {
	BeforeInsert()
	AfterInsert()
	BeforeDelete()
	AfterDelete()
	AfterFind()
	BeforeFind()
	BeforeUpdate()
	AfterUpdate()
}

func CallHook(dest interface{}, triggerType hook.HookableType, opts ...*option.CallHookUptions) {
	var destT Hookable
	var ok bool

	option := &option.CallHookUptions{}

	if len(opts) > 0 {
		option = opts[0]
	}

	if !option.IgnoreHook {
		val := reflect.ValueOf(dest)
		if val.Kind() == reflect.Ptr {
			valElem := val.Elem()
			destT, ok = dest.(Hookable)

			if valElem.Kind() == reflect.Slice || valElem.Kind() == reflect.Array {
				hookableArrayHandler(dest, triggerType)
				return
			}
		}

		if ok {
			switch triggerType {
			case hook.BeforeInsert:
				destT.BeforeInsert()
			case hook.AfterInsert:
				destT.AfterInsert()
			case hook.AfterFind:
				destT.AfterFind()
			case hook.BeforeUpdate:
				destT.BeforeUpdate()
			case hook.AfterUpdate:
				destT.AfterUpdate()
			case hook.BeforeFind:
				destT.BeforeFind()
			case hook.BeforeDelete:
				destT.BeforeDelete()
			case hook.AfterDelete:
				destT.AfterDelete()
			}

			// sub element hook
			hookableSubElementArrayHandler(dest, triggerType)
		}
	}
}

func hookableSubElementArrayHandler(dest interface{}, triggerType hook.HookableType) {
	val := reflect.ValueOf(dest)

	if val.Kind() == reflect.Ptr {
		valElem := val.Elem()
		for i := 0; i < valElem.NumField(); i++ {
			field := valElem.Field(i)
			if field.Kind() == reflect.Slice || field.Kind() == reflect.Array {
				hookableArrayHandler(field.Addr().Interface(), triggerType)
			}
		}
	}
}

func hookableArrayHandler(dest interface{}, triggerType hook.HookableType) {
	sliceValue := reflect.ValueOf(dest).Elem()
	newDest := reflect.MakeSlice(sliceValue.Type(), 0, sliceValue.Len())

	for i := 0; i < sliceValue.Len(); i++ {
		element := sliceValue.Index(i)

		destT, ok := element.Addr().Interface().(Hookable)

		if !ok {
			destI := element.Interface()
			destPointer := utils.ToPointer(destI)

			destT, ok = destPointer.(Hookable)
		}

		if ok {
			CallHook(destT, triggerType)

			newDest = reflect.Append(newDest, reflect.ValueOf(destT).Elem())
		} else {
			newDest = sliceValue
		}
	}

	sliceValue.Set(newDest)
}
