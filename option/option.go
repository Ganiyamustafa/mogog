package option

import "go.mongodb.org/mongo-driver/mongo/options"

type UpsertOptions struct {
	HookableInsert HookableInsertOptions
	HookableUpdate HookableUpdateOptions
	UpdateOptions  []*options.UpdateOptions
	Upsert         bool
	Raw            bool
}

type AggregateOptions struct {
}

type HookableInsertOptions struct {
	Model interface{}
}

type HookableUpdateOptions struct {
	Active     bool
	Model      interface{}
	IgnoreOmit []string
}

type FilterOptions struct {
	IgnoreCaseSensitive bool
}

type CallHookUptions struct {
	IgnoreOmitOnUpdate []string
	IgnoreHook         bool
}
