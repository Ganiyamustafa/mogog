package mogog

import (
	"time"

	"github.com/google/uuid"
)

type MongoBase struct {
	Hookable `bson:"-" json:"-"`
}

func (b *MongoBase) BeforeInsert() {}
func (b *MongoBase) AfterInsert()  {}
func (b *MongoBase) BeforeDelete() {}
func (b *MongoBase) AfterDelete()  {}
func (b *MongoBase) BeforeFind()   {}
func (b *MongoBase) AfterFind()    {}
func (b *MongoBase) BeforeUpdate() {}
func (b *MongoBase) AfterUpdate()  {}

type MongoBaseModel struct {
	MongoBase `bson:"-" json:"-"`
	Id        string `bson:"_id,omitempty" json:"id"`
	CreatedAt int64  `bson:"created_at" json:"created_at"`
	UpdatedAt int64  `bson:"updated_at" json:"updated_at"`
	DeletedAt int64  `bson:"deleted_at" json:"deleted_at"`
}

func (b *MongoBaseModel) BeforeInsert() {
	if b.Id == "" {
		b.Id = uuid.NewString()
	}

	b.CreatedAt = time.Now().Unix()
	b.UpdatedAt = time.Now().Unix()
}

func (b *MongoBaseModel) BeforeUpdate() {
	b.UpdatedAt = time.Now().Unix()
}

func (b *MongoBaseModel) BeforeDelete() {
	b.DeletedAt = time.Now().Unix()
}
