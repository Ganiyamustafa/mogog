package utils

import (
	"reflect"
	"strings"

	"go.mongodb.org/mongo-driver/bson"
)

func ToPointer(val interface{}) interface{} {
	pointerValue := reflect.New(reflect.TypeOf(val))
	pointerValue.Elem().Set(reflect.ValueOf(val))

	return pointerValue.Interface()
}

func IsPointer(val interface{}) bool {
	return reflect.ValueOf(val).Kind() == reflect.Ptr
}

func CreateSliceFromInterfaceSlice(sliceInterface interface{}, sliceType interface{}) interface{} {
	sliceValue := reflect.ValueOf(sliceInterface).Elem()
	sliceTypeValue := reflect.ValueOf(sliceType).Elem()
	newSlice := reflect.MakeSlice(sliceTypeValue.Type(), 0, sliceValue.Len())

	return newSlice
}

func OmitEmpty(v interface{}, ignoreField ...string) bson.M {
	updateVal := bson.M{}
	val := reflect.ValueOf(v)

	if val.Kind() == reflect.Ptr {
		val = val.Elem()
		for i := 0; i < val.NumField(); i++ {
			field := val.Type().Field(i)
			tag := strings.Split(field.Tag.Get("bson"), ",")[0]
			mogogTag := field.Tag.Get("mogog")
			updatedField := val.FieldByName(field.Name)

			if updatedField.IsValid() && !isEmptyValue(updatedField) && tag != "-" || strings.Contains(mogogTag, "omit:false") {
				updateVal[tag] = updatedField.Interface()
			}
		}
	}

	return updateVal
}

func isEmptyValue(field reflect.Value) bool {
	zero := reflect.Zero(field.Type())
	return reflect.DeepEqual(field.Interface(), zero.Interface())
}
