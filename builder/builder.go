package builder

import (
	"gitlab.com/Ganiyamustafa/mogog/operator"
	"gitlab.com/Ganiyamustafa/mogog/stages"
	"go.mongodb.org/mongo-driver/bson"
)

func Build(queries ...bson.M) []bson.M {
	return queries
}

func Project(query []ProjectQuery) bson.M {
	projectData := bson.M{}

	for _, q := range query {
		projectData[q.FieldName] = q.Value
	}

	return bson.M{
		stages.Project: projectData,
	}
}

func Match(query MatchQuery) bson.M {
	return bson.M{
		stages.Match: query.Pipeline,
	}
}

func Facet(query []FacetQuery) bson.M {
	facetData := bson.M{}

	for _, q := range query {
		facetData[q.FieldName] = q.AggregateQuery
	}

	return bson.M{
		stages.Facet: facetData,
	}
}

func Lookup(query LookupQuery) bson.M {
	return bson.M{
		stages.Lookup: query,
	}
}

func LookupPipeline(query LookupPipelineQuery) bson.M {
	return bson.M{
		stages.Lookup: query,
	}
}

func AddFields(query []AddFieldsQuery) bson.M {
	data := bson.M{}

	for _, q := range query {
		data[q.FieldName] = q.Value
	}

	return bson.M{
		stages.AddFields: data,
	}
}

func Group(query GroupQuery) bson.M {
	data := bson.M{}

	data["_id"] = query.ID

	for _, q := range query.Fields {
		data[q.Name] = q.Value
	}

	return bson.M{
		stages.Group: data,
	}
}

func Limit(query LimitQuery) bson.M {
	return bson.M{
		stages.Limit: query.Limit,
	}
}

func Skip(query SkipQuery) bson.M {
	return bson.M{
		stages.Skip: query.Skip,
	}
}

func Sort(query []SortQuery) bson.M {
	data := bson.M{}

	for _, q := range query {
		data[q.Field] = q.Sort
	}

	return bson.M{
		stages.Sort: data,
	}
}

func Filter(query FilterQuery) bson.M {
	return bson.M{
		stages.Filter: query,
	}
}

func ArrayElemAt(query ArrayElemAtQuery) bson.M {
	return bson.M{
		stages.ArrayElemAt: []interface{}{query.Array, query.Index},
	}
}

func IfNull(query IfNullQuery) bson.M {
	return bson.M{
		stages.IfNull: []interface{}{query.Value, query.IfNullValue},
	}
}

func SortArray(query SortArrayQuery) bson.M {
	return bson.M{
		stages.SortArray: query,
	}
}

func Merge(query MergeQuery) bson.M {
	return bson.M{
		stages.Merge: query,
	}
}

func Map(query MapQuery) bson.M {
	return bson.M{
		stages.Map: query,
	}
}

func MergeObjects(query MergeObjectsQuery) bson.M {
	return bson.M{
		stages.MergeObjects: []interface{}{query.Object, query.MergeObject},
	}
}

func Cond(query CondQuery) bson.M {
	return bson.M{
		stages.Cond: query,
	}
}

func Concat(strings ...string) bson.M {
	return bson.M{
		stages.Concat: strings,
	}
}

func UnionWith(query UnionWithQuery) bson.M {
	return bson.M{
		stages.UnionWith: query,
	}
}

func Unwind(query UnwindQuery) bson.M {
	return bson.M{
		stages.Unwind: query,
	}
}

func Sum(query SumQuery) bson.M {
	return bson.M{
		stages.Sum: query.Expression,
	}
}

func Paginate(query PaginateQuery) []bson.M {
	return []bson.M{
		{stages.Skip: query.Skip},
		{stages.Limit: query.Limit},
	}
}

func Divide(query DivideQuery) bson.M {
	return bson.M{
		stages.Divide: []interface{}{query.Value, query.DivisorValue},
	}
}

func Multiply(query MultiplyQuery) bson.M {
	return bson.M{
		stages.Multiply: []interface{}{query.Value, query.MultiplicationValue},
	}
}

func Round(query RoundQuery) bson.M {
	return bson.M{
		stages.Round: []interface{}{query.Value, query.Place},
	}
}

func ReplaceRoot(query ReplaceRootQuery) bson.M {
	return bson.M{
		stages.ReplaceRoot: query,
	}
}

func Search(query SearchQuery) bson.M {
	queryB, _ := bson.Marshal(query)
	data := bson.M{}

	bson.Unmarshal(queryB, &data)

	for key, value := range query.Operator {
		data[key] = value
	}

	return bson.M{
		stages.Search: data,
	}
}

func TextOperator(query TextOperatorSubQuery) bson.M {
	queryB, _ := bson.Marshal(query)
	data := bson.M{}
	bson.Unmarshal(queryB, &data)

	return bson.M{
		stages.TextOperator: data,
	}
}

func CompoundOperator(query CompoundOperatorSubQuery) bson.M {
	queryB, _ := bson.Marshal(query)
	data := bson.M{}
	bson.Unmarshal(queryB, &data)

	return bson.M{
		stages.CompoundOperator: data,
	}
}

func RegexOperator(query RegexOperatorSubQuery) bson.M {
	queryB, _ := bson.Marshal(query)
	data := bson.M{}
	bson.Unmarshal(queryB, &data)

	return bson.M{
		stages.RegexOperator: data,
	}
}

// operator
func Gt(query GtQuery) bson.M {
	return bson.M{
		operator.Gt: []interface{}{query.Value, query.GtValue},
	}
}

func Gte(query GteQuery) bson.M {
	return bson.M{
		operator.Gte: []interface{}{query.Value, query.GteValue},
	}
}

func Lt(query LtQuery) bson.M {
	return bson.M{
		operator.Lt: []interface{}{query.Value, query.LtValue},
	}
}

func Lte(query LteQuery) bson.M {
	return bson.M{
		operator.Lte: []interface{}{query.Value, query.LteValue},
	}
}

func Ne(query NeQuery) bson.M {
	return bson.M{
		operator.Ne: []interface{}{query.Value, query.NeValue},
	}
}

func In(query InQuery) bson.M {
	return bson.M{
		operator.In: []interface{}{query.Value, query.InArray},
	}
}

func Nin(query NinQuery) bson.M {
	return bson.M{
		operator.Not: In(InQuery{Value: query.Value, InArray: query.NinArray}),
	}
}

func Not(query NotQuery) bson.M {
	return bson.M{
		operator.Not: query,
	}
}

func Eq(query EqQuery) bson.M {
	return bson.M{
		operator.Eq: []interface{}{query.Value, query.EqValue},
	}
}

func Expr(query bson.M) bson.M {
	return bson.M{
		operator.Expr: query,
	}
}

func And(query []AndOrQuery) bson.M {
	matchData := []interface{}{}

	for _, q := range query {
		if q.Raw {
			matchData = append(matchData, q.Value)
		} else {
			matchData = append(matchData, bson.M{q.FieldName: q.Value})
		}
	}

	return bson.M{
		operator.And: matchData,
	}
}

func Or(query []AndOrQuery) bson.M {
	matchData := []interface{}{}

	for _, q := range query {
		if q.Raw {
			matchData = append(matchData, q.Value)
		} else {
			matchData = append(matchData, bson.M{q.FieldName: q.Value})
		}
	}

	return bson.M{
		operator.Or: matchData,
	}
}

func AddToSet(query []AddToSetQuery) bson.M {
	data := bson.M{}

	for _, q := range query {
		data[q.FieldName] = q.Value
	}

	return bson.M{
		operator.AddToSet: data,
	}
}

func Set(query []SetQuery) bson.M {
	data := bson.M{}

	for _, q := range query {
		data[q.FieldName] = q.Value
	}

	return bson.M{
		operator.Set: data,
	}
}

func Pull(query []PullQuery) bson.M {
	data := bson.M{}

	for _, q := range query {
		data[q.FieldName] = q.Value
	}

	return bson.M{
		operator.Pull: data,
	}
}
