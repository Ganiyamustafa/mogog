package builder

import "go.mongodb.org/mongo-driver/bson"

type FacetQuery struct {
	FieldName      string
	AggregateQuery []bson.M
}

type ProjectQuery struct {
	FieldName string
	Value     interface{}
}

type MatchQuery struct {
	Pipeline bson.M
}

type AndOrQuery struct {
	FieldName string
	Value     interface{}
	Raw       bool
}

type AddToSetQuery struct {
	FieldName string
	Value     interface{}
}

type PullQuery struct {
	FieldName string
	Value     interface{}
}

type LookupQuery struct {
	From         string `bson:"from"`
	LocalField   string `bson:"localField"`
	ForeignField string `bson:"foreignField"`
	As           string `bson:"as"`
}

type LookupPipelineQuery struct {
	From     string   `bson:"from"`
	Let      bson.M   `bson:"let"`
	Pipeline []bson.M `bson:"pipeline"`
	As       string   `bson:"as"`
}

type AddFieldsQuery struct {
	FieldName string
	Value     interface{}
}

type SetQuery struct {
	FieldName string
	Value     interface{}
}

// GroupQuery represents the $group stage in a MongoDB aggregation pipeline.
type GroupQuery struct {
	// ID is the expression that specifies the grouping criteria for the _id field.
	// The _id field is used to group documents based on the specified expression.
	//
	// The type of ID can be any valid MongoDB expression that results in a unique identifier.
	// This can include field names, constant values, or expressions using aggregation operators.
	//
	// Example:
	//   - "$fieldName": Groups by the value of a field.
	//   - bson.M{"$substr": []interface{}{"$fieldName", 0, 1}}: Groups by the first character of a field.
	//   - "constantValue": Groups all documents into a single group with the specified constant value.
	//
	// It is recommended to use expressions that result in unique values to effectively group documents.
	ID interface{}

	// Fields is a slice of FieldSubQuery representing the fields to include in the group.
	Fields []FieldSubQuery
}

// FieldSubQuery represents a subquery for a field within a $group stage.
type FieldSubQuery struct {
	// Name is the name of the field in the output document.
	Name string `bson:"name"`

	// value defining the value of the field. It can be expression or BSON document pipeline
	Value interface{}
}

type LimitQuery struct {
	Limit int
}

type SkipQuery struct {
	Skip int
}

type SortQuery struct {
	Field string
	Sort  int
}

// FilterQuery represents the $filter stage in a MongoDB aggregation pipeline.
type FilterQuery struct {
	// Input is the array expression to filter.
	Input interface{} `bson:"input"`

	// Cond is the BSON document that specifies the filter condition.
	Cond bson.M `bson:"cond"`

	// As is the name of the variable that represents each element of the array in the filter condition.
	As string `bson:"as,omitempty"`

	// Limit is an optional field that specifies the maximum number of matching elements to return.
	Limit int `bson:"limit,omitempty"`
}

// ArrayElemAtQuery represents the $arrayElemAt stage in a MongoDB aggregation pipeline.
type ArrayElemAtQuery struct {
	// Array is the array expression from which to extract the element. It can be an expression that resolves to an array
	Array interface{}

	// Index is the zero-based index indicating which element to extract from the array.
	Index int
}

// IfNullQuery represents the $ifNull stage in a MongoDB aggregation pipeline.
type IfNullQuery struct {
	// Value is the expression or field that is checked for a null or missing value.
	Value interface{}

	// IfNullValue is the value to return if the specified field or expression is null or missing.
	IfNullValue interface{}
}

// SortArrayQuery represents the $sortArray stage in a MongoDB aggregation pipeline.
// Example Usage:
//   query := SortArrayQuery{
//       Input:  "$arrayField",
//       SortBy: bson.M{"nestedField": -1},
//   }
// Another Example:
//   query := SortArrayQuery{
//       Input:  []int{1, 2, 3, 4},
//       SortBy: 1,
//   }
type SortArrayQuery struct {
	// Input specifies the array to sort. It can be an expression that resolves to an array.
	Input interface{} `bson:"input"`

	// SortBy specifies the criteria to use when sorting the array.
	// It can be a field name, an expression, or a document that represents the sort order.
	SortBy interface{} `bson:"sortBy"`
}

// MergeQuery represents the $merge stages in a MongoDB aggregation pipeline.
type MergeQuery struct {
	// Into specifies the name of the output collection where the results will be stored.
	// The output documents are merged into this collection or view.
	Into string `bson:"into"`

	// WhenMatched specifies the behavior when a document in the input stream matches
	// an existing document in the output collection.
	// Possible values: "merge", "discard", "fail".
	//   - "merge": Updates the existing document with the input document.
	//   - "discard": Discards the input document without updating the existing document.
	//   - "fail": Fails the operation if a match is found.
	WhenMatched string `bson:"whenMatched"`

	// WhenNotMatched specifies the behavior when a document in the input stream
	// does not match any existing document in the output collection.
	// Possible values: "insert", "discard", "fail".
	//   - "insert": Inserts the input document into the output collection.
	//   - "discard": Discards the input document.
	//   - "fail": Fails the operation if no match is found.
	WhenNotMatched string `bson:"whenNotMatched"`
}

// MapQuery represents the $map stage in a MongoDB aggregation pipeline.
type MapQuery struct {
	// Input is the array expression to apply the $map operation.
	Input interface{} `bson:"input"`

	// As is an optional field that specifies the name of the variable to represent each element of the array.
	// If not provided, the variable name defaults to "this".
	As string `bson:"as,omitempty"`

	// In is the expression that specifies the transformation to apply to each element of the array.
	In bson.M `bson:"in"`
}

// MergeObjectsQuery represents the $mergeObjects stage in a MongoDB aggregation pipeline.
type MergeObjectsQuery struct {
	// Object is the expression or document to merge with the MergeObject.
	Object interface{}

	// MergeObject is the expression or document to merge with the Object.
	MergeObject interface{}
}

// CondQuery represents the $cond stage in a MongoDB aggregation pipeline.
type CondQuery struct {
	// If is the expression that evaluates to a boolean determining whether to return the "then" or "else" expression.
	If interface{} `bson:"if"`

	// Then is the expression to return if the "if" expression evaluates to true.
	Then interface{} `bson:"then"`

	// Else is the expression to return if the "if" expression evaluates to false.
	Else interface{} `bson:"else"`
}

// UnionWithQuery represents the $unionWith stage in a MongoDB aggregation pipeline.
type UnionWithQuery struct {
	// Coll is the name of the collection to union with.
	Coll string `bson:"coll"`

	// Pipeline is an array of pipeline stages to apply to the union operation.
	Pipeline []bson.M `bson:"pipeline"`
}

// Unwind represents the $unwind stage in a MongoDB aggregation pipeline.
type UnwindQuery struct {
	// Field is the field path to unwind.
	Path string `bson:"path"`

	// IncludeArrayIndex, if provided, is the name of the new field that will contain the index of the element in the array.
	// It is optional, and if not specified, the array index will not be included in the output documents.
	IncludeArrayIndex string `bson:"includeArrayIndex,omitempty"`

	// PreserveNullAndEmptyArrays specifies whether to include documents with empty arrays in the output.
	// If set to true, documents with empty arrays will be included; otherwise, they will be excluded.
	PreserveNullAndEmptyArrays bool `bson:"preserveNullAndEmptyArrays"`
}

// SumQuery represents the $sum stage in a MongoDB aggregation pipeline.
type SumQuery struct {
	// Expression is the expression or field for which to calculate the sum.
	Expression interface{}
}

// PaginateQuery represents the parameters for paginating results in a MongoDB query.
type PaginateQuery struct {
	// Limit is the maximum number of documents to return in a single page. represent $limit stages
	Limit int

	// Skip is the number of documents to skip before starting to return documents in a page. represent $skip stages
	Skip int
}

// DivideQuery represents the $divide stage in a MongoDB aggregation pipeline.
type DivideQuery struct {
	// Value is the dividend expression or field.
	Value interface{}

	// DivisorValue is the divisor expression or field.
	DivisorValue interface{}
}

// MultiplyQuery represents the $multiply stage in a MongoDB aggregation pipeline.
type MultiplyQuery struct {
	// Value is the first operand expression or field for multiplication.
	Value interface{}

	// MultiplicationValue is the second operand expression or field for multiplication.
	MultiplicationValue interface{}
}

// Round represents the $round stage in a MongoDB aggregation pipeline.
type RoundQuery struct {
	// Value is the expression or field to round.
	Value interface{}

	// Place is the number of decimal places to round to.
	// If not specified or set to 0, the value will be rounded to the nearest integer.
	Place int
}

// ReplaceRoot represents the $replaceRoot stage in a MongoDB aggregation pipeline.
type ReplaceRootQuery struct {
	// NewRoot is the expression or field that becomes the new root of the document.
	NewRoot interface{} `bson:"newRoot"`
}

// EqQuery represents the $eq stage in a MongoDB aggregation pipeline.
type EqQuery struct {
	// Value is the expression or field to compare.
	Value interface{}

	// EqValue is the value to compare with the specified expression or field.
	EqValue interface{}
}

// InQuery represents the $in stage in a MongoDB aggregation pipeline.
type InQuery struct {
	// Value is the expression or field to check for inclusion in the array.
	Value interface{}

	// InArray is the array of values to check for inclusion.
	InArray interface{}
}

// NinQuery represents the $not and $in stage in a MongoDB aggregation pipeline.
type NinQuery struct {
	// Value is the expression or field to check for exclusion from the array.
	Value interface{}

	// NinArray is the array of values to check for exclusion.
	NinArray interface{}
}

// NotQuery represents the $not stage in a MongoDB aggregation pipeline.
type NotQuery struct {
	// Expression is the expression or condition to negate.
	Expression interface{}
}

// NeQuery represents the $ne stage in a MongoDB aggregation pipeline.
type NeQuery struct {
	// Value is the expression or field to compare.
	Value interface{}

	// NeValue is the value to compare for inequality.
	NeValue interface{}
}

// GtQuery represents the $gt stage in a MongoDB aggregation pipeline.
type GtQuery struct {
	// Value is the expression or field to compare.
	Value interface{}

	// GtValue is the value to compare for greater than (>) condition.
	GtValue interface{}
}

// GteQuery represents the $gte stage in a MongoDB aggregation pipeline.
type GteQuery struct {
	// Value is the expression or field to compare.
	Value interface{}

	// GteValue is the value to compare for greater than or equal to (>=) condition.
	GteValue interface{}
}

// LtQuery represents the $lt stage in a MongoDB aggregation pipeline.
type LtQuery struct {
	// Value is the expression or field to compare.
	Value interface{}

	// LtValue is the value to compare for less than (<) condition.
	LtValue interface{}
}

// LteQuery represents the $lte stage in a MongoDB aggregation pipeline.
type LteQuery struct {
	// Value is the expression or field to compare.
	Value interface{}

	// LteValue is the value to compare for less than or equal to (<=) condition.
	LteValue interface{}
}

// SearchQuery represents a MongoDB aggregation pipeline stage for text search using the $search operator.
// A $search pipeline stage has the following prototype form:
//
// Usage:
//   query := SearchQuery{
//       Index: "default",
//       OperatorName: "text",
//       OperatorSpecification: bson.M{
//           "query": "search term",
//           "path":  "path1",
//       },
//       Highlight: highlighOptionsSubQuery{
//           Path: "field_to_highlight",
//       },
//       Concurrent:         true,
//       Count: countOptionsSubQuery{
//           Type:      "total",
//           Threshold: "10",
//       },
//       ScoreDetails:       true,
//       Sort:               bson.M{"score": 1},
//       ReturnStoredSource: false,
//       Tracking: trackingOptionsSubQuery{
//           SearchTerms: "tracking_terms",
//       },
//   }
type SearchQuery struct {
	// Index is the name of the search index to use.
	Index string `bson:"index,omitempty"`

	// Operator is a field in the SearchQuery structure representing the name of the text search operator.
	// The text search operator determines the type of search operation to be performed.
	Operator bson.M `bson:"-"`

	// Highlight specifies the field or fields to highlight in the search results.
	Highlight *highlighOptionsSubQuery `bson:"highlight,omitempty"`

	// Concurrent specifies whether to perform the text search concurrently on each shard.
	Concurrent bool `bson:"concurrent"`

	// Count specifies the additional count options for the text search.
	Count *countOptionsSubQuery `bson:"count,omitempty"`

	// ScoreDetails specifies whether to include score details in the search results.
	ScoreDetails bool `bson:"scoreDetails,omitempty"`

	// Sort specifies the sorting criteria for the search results.
	// {<fields-to-sort>: 1 | -1}
	Sort bson.M `bson:"sort,omitempty"`

	// ReturnStoredSource specifies whether to include the stored source document in the search results.
	ReturnStoredSource bool `bson:"returnStoredSource"`

	// Tracking specifies additional tracking options for the text search.
	Tracking *trackingOptionsSubQuery `bson:"tracking"`
}

// countOptionsSubQuery represents the additional count options for the text search in SearchQuery.
type countOptionsSubQuery struct {
	// Type specifies the type of counting to perform. Valid values: "total", "lowerBound".
	Type string `bson:"type,omitempty"`

	// Threshold specifies the threshold for count operations.
	Threshold string `bson:"threshold,omitempty"`
}

// highlighOptionsSubQuery represents the highlighting options for the text search in SearchQuery.
type highlighOptionsSubQuery struct {
	// Path specifies the field or fields to highlight.
	Path string `bson:"path,omitempty"`

	// MaxCharsToExamine specifies the maximum number of characters to examine when highlighting.
	MaxCharsToExamine int `bson:"maxCharsToExamine,omitempty"`

	// MaxNumPassages specifies the maximum number of passages to include in the search results.
	MaxNumPassages int `bson:"maxNumPassages,omitempty"`
}

// trackingOptionsSubQuery represents additional tracking options for the text search in SearchQuery.
type trackingOptionsSubQuery struct {
	// SearchTerms specifies the search terms for tracking purposes.
	SearchTerms string `bson:"searchTerms,omitempty"`
}

// TextOperatorSubQuery represents the detailed configuration for text search operators like "text", "phrase", "term", "autocomplete", "wildcard", and "regex".
// It provides specific parameters for the text search operation.
type TextOperatorSubQuery struct {
	// Query is the search term or expression to match.
	Query interface{} `bson:"query"`

	// Path specifies the field or fields on which the text search should be applied.
	Path interface{} `bson:"path"`

	// Fuzzy provides optional parameters for fuzzy searches. It is only applicable to certain text search operators.
	Fuzzy *fuzzyOptionsSubQuery `bson:"fuzzy,omitempty"`

	// Synonyms is an optional parameter that allows specifying synonym sets for the text search.
	Synonyms string `bson:"synonyms,omitempty"`
}

// fuzzyOptionsSubQuery represents the optional parameters for fuzzy text searches, applicable to certain text search operators.
type fuzzyOptionsSubQuery struct {
	// MaxEdits is the maximum number of single-character edits (insertions, deletions, or substitutions) allowed in the search term.
	MaxEdits int `bson:"maxEdits,omitempty"`

	// PrefixLength is the number of characters at the beginning of the term that must exactly match for the term to be considered a match.
	PrefixLength int `bson:"prefixLength,omitempty"`

	// MaxExpansions is the maximum number of variations for each fuzzy term. It limits the number of variations considered during the search.
	MaxExpansions int `bson:"maxExpansions,omitempty"`
}

// CompoundOperatorSubQuery represents a subquery for compound text search operators.
// It allows combining multiple text search conditions using boolean logic (AND, OR, NOT).
// This is typically used in the "compound" text search operator.
type CompoundOperatorSubQuery struct {
	// Must specifies conditions that must all be satisfied for a document to be considered a match.
	Must []bson.M `bson:"must,omitempty"`

	// MustNot specifies conditions that must not be satisfied for a document to be considered a match.
	MustNot []bson.M `bson:"mustNot,omitempty"`

	// Should specifies conditions where at least one must be satisfied for a document to be considered a match.
	Should []bson.M `bson:"should,omitempty"`

	// Filter specifies additional conditions that act as filters but do not impact the score of the document.
	Filter []bson.M `bson:"filter,omitempty"`

	MinimumShouldMatch int `bson:"minimumShouldMatch,omitempty"`
}

// RegexOperatorSubQuery represents the detailed configuration for the regex text search operator.
// It provides parameters for performing regular expression-based searches on specified fields.
type RegexOperatorSubQuery struct {
	// Query is the regular expression pattern to match against the field values.
	Query interface{} `bson:"query"`

	// Path specifies the field or fields on which the regex search should be applied.
	Path interface{} `bson:"path"`

	// AllowAnalyzedField specifies whether to allow using an analyzed field for regex matching.
	// If set to true, the regex operator can match against fields that have undergone text analysis.
	AllowAnalyzedField bool `bson:"allowAnalyzedField"`
}
