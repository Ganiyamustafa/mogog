package mogog

import (
	"context"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.uber.org/zap"
)

func NewConnection(opts ...*options.ClientOptions) *Mogog {
	client, err := mongo.Connect(context.TODO(), opts...)
	if err != nil {
		zap.S().Fatal(err)
	}

	zap.S().Infof("Connected to Mongo successfully!")

	return &Mogog{
		Client:  client,
		context: context.Background(),
	}
}
