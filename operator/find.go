package operator

// comparison
const (
	Ne  = "$ne"
	Eq  = "$eq"
	Nin = "$nin"
	Gte = "$gte"
	Lte = "$lte"
	Gt  = "$gt"
	Lt  = "$lt"
	In  = "$in"
)

// logical
const (
	And = "$and"
	Not = "$not"
	Nor = "$nor"
	Or  = "$or"
)

// element
const (
	Exists = "$exists"
	Type   = "$type"
)

// evaluation
const (
	Expr       = "$expr"
	JsonSchema = "$jsonSchema"
	Mod        = "$mod"
	Regex      = "$regex"
	Text       = "$text"
	Where      = "$where"
)

// geospatial
const (
	GeoIntersects = "$geoIntersects"
	GeoWithIn     = "$geoWithIn"
	Near          = "$near"
	NearSphere    = "$nearSphere"
)

// Array
const (
	All       = "$all"
	ElemMatch = "$elemMatch"
	Size      = "$size"
	Slice     = "$slice"
)

// Bitwise
const (
	BitsAllClear = "$bitsAllClear"
	BitsAllSet   = "$bitsAllSet"
	BitsAnyClear = "$bitsAnyClear"
	BitsAnySet   = "$bitsAnySet"
)
