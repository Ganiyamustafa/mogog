package operator

// fields
const (
	CurrentDate = "$currentDate"
	Inc         = "$inc"
	Min         = "$min"
	Max         = "$max"
	Mul         = "$mul"
	Rename      = "$rename"
	Set         = "$set"
	SetOnInsert = "$setOnInsert"
	Unset       = "$unset"
)

// Array
const (
	AddToSet = "$addToSet"
	Pop      = "$pop"
	Pull     = "$pull"
	Push     = "$push"
	PullAll  = "$pullAll"
)

// Modifiers
const (
	Each     = "$each"
	Position = "$position"
	Sort     = "$sort"
)

// Bitwise
const (
	Bit = "$bit"
)
