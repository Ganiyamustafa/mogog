package mogog

import (
	"errors"

	"github.com/jinzhu/copier"
	jsoniter "github.com/json-iterator/go"
	"gitlab.com/Ganiyamustafa/mogog/hook"
	"gitlab.com/Ganiyamustafa/mogog/option"
	"gitlab.com/Ganiyamustafa/mogog/utils"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type updateType int

const (
	updateOne updateType = iota
	updateMany
)

func (t *Mogog) resetFilter() {
	t.filter = bson.M{}
	t.Pipe = []bson.M{}
}

func (t *Mogog) insertOnUpsert(data interface{}, option *option.UpsertOptions) (result interface{}, err error) {
	var insertData interface{}

	if option.HookableInsert.Model == nil {
		option.HookableInsert.Model = data
	}

	if !utils.IsPointer(option.HookableInsert.Model) {
		insertData = utils.ToPointer(option.HookableInsert.Model)
	} else {
		insertData = option.HookableInsert.Model
	}

	copier.CopyWithOption(insertData, data, copier.Option{IgnoreEmpty: true})

	result, err = t.InsertOne(insertData)

	return result, err
}

func (t *Mogog) updateOnUpsert(data interface{}, opt *option.UpsertOptions, updateType updateType) (result *mongo.UpdateResult, err error) {
	var updateData interface{}

	if opt.HookableUpdate.Model == nil {
		opt.HookableUpdate.Model = data
	}

	updateData = opt.HookableUpdate.Model

	if !utils.IsPointer(opt.HookableInsert.Model) {
		updateData = utils.ToPointer(updateData)
	}

	if !opt.Raw && opt.HookableUpdate.Active {
		CallHook(updateData, hook.BeforeUpdate, &option.CallHookUptions{IgnoreOmitOnUpdate: opt.HookableUpdate.IgnoreOmit})
	}

	copier.CopyWithOption(updateData, data, copier.Option{IgnoreEmpty: true})

	query, err := t.generateQueryOnUpsert(updateData, opt)

	if err != nil {
		return nil, err
	}

	opt.UpdateOptions = append(opt.UpdateOptions, options.Update().SetUpsert(opt.Upsert))

	switch updateType {
	case updateOne:
		result, err = t.coll.UpdateOne(t.context, t.filter, query, opt.UpdateOptions...)
	case updateMany:
		result, err = t.coll.UpdateMany(t.context, t.filter, query, opt.UpdateOptions...)
	}

	if opt.HookableUpdate.Active {
		CallHook(updateData, hook.AfterUpdate)
	}

	return result, err
}

func (t *Mogog) generateQueryOnUpsert(data interface{}, option *option.UpsertOptions) (query bson.M, err error) {
	if option.Raw {
		// If raw option is true, assume data is already a bson.M or *bson.M
		switch v := data.(type) {
		case bson.M:
			query = v
		case *bson.M:
			query = *v
		default:
			return nil, errors.New("raw value only receives bson.M or *bson.M objects")
		}
	} else {
		// Use $set with OmitEmpty to create the query
		query = bson.M{"$set": utils.OmitEmpty(data)}
	}

	return query, nil
}

func (t *Mogog) jsonAlias() jsoniter.API {
	if t.jsoniter == nil {
		t.jsoniter = aliasJson{}.init("bson")
	}
	return t.jsoniter
}
