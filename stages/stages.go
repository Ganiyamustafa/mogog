package stages

const (
	Project      = "$project"
	Match        = "$match"
	Facet        = "$facet"
	Lookup       = "$lookup"
	AddFields    = "$addFields"
	Group        = "$group"
	Limit        = "$limit"
	Skip         = "$skip"
	Sort         = "$sort"
	Filter       = "$filter"
	ArrayElemAt  = "$arrayElemAt"
	IfNull       = "$ifNull"
	SortArray    = "$sortArray"
	Merge        = "$merge"
	Map          = "$map"
	MergeObjects = "$mergeObjects"
	Cond         = "$cond"
	Concat       = "$concat"
	UnionWith    = "$unionWith"
	Unwind       = "$unwind"
	Sum          = "$sum"
	Divide       = "$divide"
	Multiply     = "$multiply"
	Round        = "$round"
	ReplaceRoot  = "$replaceRoot"
	Type         = "$type"
	Search       = "$search"
)

const (
	TextOperator     = "text"
	CompoundOperator = "compound"
	RegexOperator    = "regex"
)
