package mogog

import jsoniter "github.com/json-iterator/go"

type aliasJson jsoniter.Config

func (t aliasJson) init(aliasTag string) jsoniter.API {
	return jsoniter.Config{
		EscapeHTML:             true,
		SortMapKeys:            true,
		ValidateJsonRawMessage: true,
		TagKey:                 aliasTag,
	}.Froze()
}
