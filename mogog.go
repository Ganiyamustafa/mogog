package mogog

import (
	"context"
	"fmt"

	"github.com/jinzhu/copier"
	jsoniter "github.com/json-iterator/go"
	"gitlab.com/Ganiyamustafa/mogog/hook"
	"gitlab.com/Ganiyamustafa/mogog/option"
	"gitlab.com/Ganiyamustafa/mogog/utils"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Mogog struct {
	Client   *mongo.Client
	filter   bson.M
	Pipe     []bson.M
	db       *mongo.Database
	coll     *mongo.Collection
	context  context.Context
	jsoniter jsoniter.API
}

func NewMogog(t *Mogog) *Mogog {
	newMogog := &Mogog{}
	if t.context == nil {
		t.context = context.Background()
	}
	copier.CopyWithOption(&newMogog, &t, copier.Option{IgnoreEmpty: true})

	return newMogog
}

func (t *Mogog) ConnectDatabase(dbName string, opts ...*options.DatabaseOptions) *Mogog {
	t.db = t.Client.Database(dbName, opts...)
	return NewMogog(t)
}

func (t *Mogog) ConnectCollection(collName string, opts ...*options.CollectionOptions) *Mogog {
	t.coll = t.db.Collection(collName, opts...)
	return NewMogog(t)
}

func (t *Mogog) SetContext(ctx context.Context) *Mogog {
	t.context = ctx
	return t
}

func (t *Mogog) Pipeline(pipeline []bson.M) *Mogog {
	t.Pipe = pipeline
	return t
}

func (t *Mogog) Aggregate(dest interface{}, opts ...*options.AggregateOptions) error {
	defer t.resetFilter()

	var results []bson.M

	CallHook(dest, hook.BeforeFind)

	cursor, err := t.coll.Aggregate(t.context, t.Pipe, opts...)

	if err != nil {
		return err
	}

	for cursor.Next(context.Background()) {
		var result bson.M
		err := cursor.Decode(&result)
		if err != nil {
			return err
		}

		results = append(results, result)
	}

	bsonB, _ := t.jsonAlias().Marshal(&results)
	err = t.jsonAlias().Unmarshal(bsonB, dest)

	if err != nil {
		return err
	}

	CallHook(dest, hook.AfterFind)

	return nil
}

func (t *Mogog) Filter(query interface{}, opts ...*option.FilterOptions) *Mogog {
	var documentMap bson.M

	bsonB, _ := bson.Marshal(query)
	bson.Unmarshal(bsonB, &documentMap)

	option := &option.FilterOptions{}

	if len(opts) > 0 {
		option = opts[0]
	}

	if option.IgnoreCaseSensitive {
		for key, val := range documentMap {
			t.filter[key] = bson.M{
				"$regex":   fmt.Sprintf("^%v$", val),
				"$options": "i",
			}
		}
		return t
	}

	t.filter = documentMap

	return t
}

func (t *Mogog) CreateCollection(collName string, opts ...*options.CreateCollectionOptions) error {
	return t.db.CreateCollection(t.context, collName, opts...)
}

func (t *Mogog) DeleteCollection(collName string) error {
	return t.ConnectCollection(collName).coll.Drop(t.context)
}

func (t *Mogog) First(dest interface{}, opts ...*options.FindOneOptions) (err error) {
	defer t.resetFilter()

	CallHook(dest, hook.BeforeFind)

	err = t.coll.FindOne(t.context, t.filter, opts...).Decode(dest)

	CallHook(dest, hook.AfterFind)

	return
}

func (t *Mogog) Find(dest interface{}, opts ...*options.FindOptions) (err error) {
	defer t.resetFilter()

	var results []bson.M

	CallHook(dest, hook.BeforeFind)

	cursor, err := t.coll.Find(t.context, t.filter, opts...)

	if err != nil {
		return err
	}

	for cursor.Next(context.Background()) {
		var result bson.M
		err := cursor.Decode(&result)
		if err != nil {
			return err
		}

		results = append(results, result)
	}

	bsonB, _ := t.jsonAlias().Marshal(&results)
	t.jsonAlias().Unmarshal(bsonB, dest)

	CallHook(dest, hook.AfterFind)

	return
}

func (t *Mogog) CountDocuments() (int64, error) {
	return t.coll.CountDocuments(t.context, t.filter)
}

func (t *Mogog) InsertOne(data interface{}, opts ...*options.InsertOneOptions) (result *mongo.InsertOneResult, err error) {
	if !utils.IsPointer(data) {
		data = utils.ToPointer(data)
	}

	CallHook(data, hook.BeforeInsert)

	result, err = t.coll.InsertOne(t.context, data, opts...)

	CallHook(data, hook.AfterInsert)

	return result, err
}

func (t *Mogog) InsertMany(data []interface{}, opts ...*options.InsertManyOptions) (result *mongo.InsertManyResult, err error) {
	var newData interface{}
	if !utils.IsPointer(data) {
		newData = utils.ToPointer(data)
	}

	CallHook(newData, hook.BeforeInsert)

	result, err = t.coll.InsertMany(t.context, *newData.(*[]interface{}), opts...)

	CallHook(newData, hook.AfterInsert)

	return result, err
}

func (t *Mogog) UpdateOne(data interface{}, opts ...*option.UpsertOptions) (result interface{}, err error) {
	defer t.resetFilter()

	option := &option.UpsertOptions{Upsert: false, UpdateOptions: []*options.UpdateOptions{options.Update()}}

	if len(opts) > 0 {
		option = opts[0]
	}

	updateRes, err := t.updateOnUpsert(data, option, updateOne)

	if err != nil {
		return nil, err
	}

	if (updateRes.MatchedCount == 0) && option.Upsert {
		return t.insertOnUpsert(data, option)
	}

	return updateRes, err
}

func (t *Mogog) UpdateMany(data interface{}, opts ...*option.UpsertOptions) (result interface{}, err error) {
	defer t.resetFilter()

	option := &option.UpsertOptions{Upsert: false, UpdateOptions: []*options.UpdateOptions{options.Update()}}

	if len(opts) > 0 {
		option = opts[0]
	}

	updateRes, err := t.updateOnUpsert(data, option, updateMany)

	if err != nil {
		return nil, err
	}

	if (updateRes.MatchedCount == 0) && option.Upsert {
		return t.insertOnUpsert(data, option)
	}

	return updateRes, err
}

func (t *Mogog) DeleteOne(model interface{}, opts ...*options.DeleteOptions) (result interface{}, err error) {
	CallHook(model, hook.BeforeDelete)

	t.coll.DeleteOne(t.context, t.filter, opts...)

	CallHook(model, hook.AfterDelete)
	return
}

func (t *Mogog) DeleteMany(model interface{}, opts ...*options.DeleteOptions) (result interface{}, err error) {
	CallHook(model, hook.BeforeDelete)

	t.coll.DeleteMany(t.context, t.filter, opts...)

	CallHook(model, hook.AfterDelete)
	return
}

func (t *Mogog) StartSession(fn func() error) (mongo.Session, error) {
	return t.Client.StartSession()
}

func (t *Mogog) ExecTransaction(fn func(sc mongo.SessionContext) error) error {
	session, err := t.Client.StartSession()

	if err != nil {
		return err
	}

	if _, err := session.WithTransaction(context.Background(), func(sc mongo.SessionContext) (interface{}, error) {
		err := fn(sc)

		if err != nil {
			session.AbortTransaction(sc)
		} else {
			session.CommitTransaction(sc)
		}

		session.EndSession(sc)

		return nil, err
	}); err != nil {
		return err
	}

	return err
}

// removed soon

func (t *Mogog) UpsertOne(data interface{}, opts ...*option.UpsertOptions) (result interface{}, err error) {
	defer t.resetFilter()

	option := &option.UpsertOptions{Upsert: true, UpdateOptions: []*options.UpdateOptions{options.Update()}}

	if len(opts) > 0 {
		option = opts[0]
	}

	updateRes, err := t.updateOnUpsert(data, option, updateOne)

	if err != nil {
		return nil, err
	}

	if (updateRes.MatchedCount == 0) && option.Upsert {
		return t.insertOnUpsert(data, option)
	}

	return updateRes, err
}

func (t *Mogog) UpsertMany(data interface{}, opts ...*option.UpsertOptions) (result interface{}, err error) {
	defer t.resetFilter()

	option := &option.UpsertOptions{Upsert: true, UpdateOptions: []*options.UpdateOptions{options.Update()}}

	if len(opts) > 0 {
		option = opts[0]
	}

	updateRes, err := t.updateOnUpsert(data, option, updateMany)

	if err != nil {
		return nil, err
	}

	if (updateRes.MatchedCount == 0) && option.Upsert {
		return t.insertOnUpsert(data, option)
	}

	return updateRes, err
}
