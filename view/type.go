package view

import (
	"gitlab.com/Ganiyamustafa/mogog"
	"go.mongodb.org/mongo-driver/bson"
)

type ViewBuilder struct {
	Mogog *mogog.Mogog
	Data  ViewData
}

type ViewData struct {
	Name           string   `bson:"name" json:"name"`
	Source         string   `bson:"source" json:"source"`
	DatabaseSource string   `bson:"database_source" json:"database_source"`
	Pipeline       []bson.M `bson:"pipeline" json:"pipeline"`
}
