package view

import (
	"gitlab.com/Ganiyamustafa/mogog"
	"gitlab.com/Ganiyamustafa/mogog/builder"
	"gitlab.com/Ganiyamustafa/mogog/option"
	"go.mongodb.org/mongo-driver/bson"
)

func CreateView(data ViewData, mogog *mogog.Mogog) {
	mogog.ConnectDatabase("mogog").ConnectCollection("views").Filter(bson.M{"name": data.Name}).UpdateOne(data, &option.UpsertOptions{Upsert: true})
}

func GetView(name string, mogog *mogog.Mogog) *ViewBuilder {
	viewData := ViewData{}
	viewBuilder := ViewBuilder{
		Mogog: mogog,
	}

	mogog.ConnectDatabase("mogog").ConnectCollection("views").Filter(bson.M{"name": name}).First(&viewData)

	viewBuilder.Data = viewData

	return &viewBuilder
}

func (t *ViewBuilder) LookupPipeline(addPipeline ...bson.M) bson.M {
	return builder.LookupPipeline(builder.LookupPipelineQuery{
		From:     t.Data.Source,
		Pipeline: append(t.Data.Pipeline, addPipeline...),
		Let:      bson.M{},
		As:       t.Data.Name,
	})
}

func (t *ViewBuilder) Exec(dest interface{}, addPipeline ...bson.M) error {
	return t.Mogog.ConnectDatabase(t.Data.DatabaseSource).ConnectCollection(t.Data.Source).Pipeline(append(t.Data.Pipeline, addPipeline...)).Aggregate(dest)
}
