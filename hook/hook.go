package hook

type HookableType int

const (
	BeforeInsert HookableType = iota
	AfterInsert
	BeforeUpdate
	AfterUpdate
	BeforeFind
	AfterFind
	BeforeDelete
	AfterDelete
)
